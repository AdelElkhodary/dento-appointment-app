/**
 * Get time slots list
 * @param {object} state - The vuex state object
 * @return {object[]} time slots list
 */
export const getTimeSlotsList = state => state.timeSlotsList;

/**
 * Get date range
 * @param {object} state - The vuex state object
 * @return {object} date range
 */
export const getDateRange = state => state.dateRange;

/**
 * Get time slots selected
 * @param {object} state - The vuex state object
 * @return {array[]} Time slots selected
 */
export const getTimeSlotsSelected = state => state.timeSlotsListSelected;

/**
 * Get booking status if user click on book now c2a
 * @param {object} state - The vuex state object
 * @return {boolean} booking status for c2a
 */
export const getBookingStatus = state => state.readyForBooking;

/**
 * Get loading status
 * @param {object} state - The vuex state object
 * @return {boolean} loading status
 */
export const getLoadingStatus = state => state.isLoading;
