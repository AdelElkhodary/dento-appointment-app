import * as actions from './actions';
import * as getters from './getters';
import {
  SET_TIME_SLOTS_LIST,
  SET_TIME_SLOTS_LIST_SELECTED,
  SET_READY_FOR_BOOKING_STATUS,
  SET_LOADING_STATUS,
  SET_TIME_SLOT_ID,
  SET_DATE_RANGE
} from '../mutation-types';


const mutations = {
  [SET_TIME_SLOTS_LIST](state, payload) {
    state.timeSlotsList = payload
  },
  [SET_TIME_SLOTS_LIST_SELECTED](state, payload) {
    state.timeSlotsListSelected = payload
  },
  [SET_READY_FOR_BOOKING_STATUS](state, payload) {
    state.readyForBooking = payload
  },
  [SET_LOADING_STATUS](state, payload) {
    state.isLoading = payload
  },
  [SET_TIME_SLOT_ID](state, payload) {
    state.timeSlotId = payload
  },
  [SET_DATE_RANGE](state, payload) {
    state.dateRange = payload
  }
};

const state = () => ({
  timeSlotsList: null,
  timeSlotsListSelected: [],
  readyForBooking: false,
  isLoading: false,
  timeSlotId: null,
  dateRange: {
    start: "",
    end: "",
  }
});

export default {
  mutations,
  actions,
  getters,
  state,
};
