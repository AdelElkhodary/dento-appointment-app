import API from '@/services/api'
import { URLS } from "@/services/urls";
import {
  SET_READY_FOR_BOOKING_STATUS,
  SET_TIME_SLOTS_LIST,
  SET_TIME_SLOTS_LIST_SELECTED,
  SET_LOADING_STATUS,
  SET_TIME_SLOT_ID, SET_DATE_RANGE
} from '../mutation-types'


/**
 * Fetch all time slots according to the give date range
 * @param {function} commit - The vuex commit function
 * @param {Object}  state - The vuex state object
 * @param {string} payload - from data & to date
 * @return {object[]} list of time slots with giving dates
 */
export async function fetchTimeSlotsList({state, commit}, payload) {
  const { TIME_SLOTS_LIST } = URLS
  const { data } = await API.get(TIME_SLOTS_LIST(payload));
  commit(SET_TIME_SLOTS_LIST, data)
}

/**
 * Store selected time slots date with their time slots
 * @param {function} commit - The vuex commit function
 * @param {Object}  state - The vuex state object
 * @param {array} payload - selected time slots depends on the selected date
 * @return {array[]} selected time slots depends on the selected date
 */
export async function storeTimeSlotsSelected({state, commit}, payload) {
  commit(SET_TIME_SLOTS_LIST_SELECTED, payload)
}

/**
 * Set date range values
 * @param {function} commit - The vuex commit function
 * @param {Object}  state - The vuex state object
 * @param {object} payload - date range object
 * @return {object} date range object
 */
export async function setDateRange({state, commit}, payload) {
  commit(SET_DATE_RANGE, payload)
}

/**
 * Set ready for booking button status
 * @param {function} commit - The vuex commit function
 * @param {Object}  state - The vuex state object
 * @param {boolean} payload - boolean value if the user click on time slot
 * @return {boolean} boolean value if the user click on time slot
 */
export async function setReadyForBookingStatus({state, commit}, payload) {
  commit(SET_READY_FOR_BOOKING_STATUS, payload)
}

/**
 * Set loading status when selecting date range
 * @param {function} commit - The vuex commit function
 * @param {Object}  state - The vuex state object
 * @param {boolean} payload - loading status
 * @return {boolean} loading status
 */
export async function setLoadingStatus({state, commit}, payload) {
  commit(SET_LOADING_STATUS, payload)
}

/**
 * Set time slot id
 * @param {function} commit - The vuex commit function
 * @param {Object}  state - The vuex state object
 * @param {Number|String} payload - time slot id
 * @return {Number|String} time slot id
 */
export async function setTimeSlotId({state, commit}, payload) {
  commit(SET_TIME_SLOT_ID, payload)
}

/**
 * Book time slot
 * @param {function} commit - The vuex commit function
 * @param {Object}  state - The vuex state object
 * @param {object} payload - Patient information
 * @return {boolean} loading status
 */
export async function bookTimeSlot({state, commit}, payload) {
  const { timeSlotId } = state
  const { TIME_SLOTS_UPDATE } = URLS
  const { data: {booked} } = await API.patch(TIME_SLOTS_UPDATE(timeSlotId), payload);
  return Promise.resolve(booked)
}
