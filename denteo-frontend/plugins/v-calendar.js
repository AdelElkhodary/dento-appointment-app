import Vue from 'vue';
import VCalendar from 'v-calendar';

Vue.use(VCalendar, {
  componentPrefix: 'ui',  // Use <ui-calendar />
});
