export const URLS = {
  TIME_SLOTS_LIST: ({start, end}) => `/time_slots?from=${start}&to=${end}`,
  TIME_SLOTS_UPDATE: id => `/time_slots/${id}`
}
