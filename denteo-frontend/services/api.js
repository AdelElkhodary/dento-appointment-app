import axios from 'axios';

const URL = 'http://localhost:4000'
const API = axios.create({baseURL: URL});

API.interceptors.request.use(config => config, error => Promise.reject(error));

export default API
