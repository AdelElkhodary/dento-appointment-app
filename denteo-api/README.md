# README

Booking Appoitment API 


* Ruby version -> `ruby 3.0.1`

* Rails version -> `Rails 6.1.3.1`

## Installation
```bash
$ bundle install

$ rails db:create

$ rails db:migrate

$ rails db:seed

$ rails s --port 4000
```
