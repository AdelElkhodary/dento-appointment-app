# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

times = []

(8..18).each do |hour|
    if hour < 10
        hour = "0#{hour}"
    end

    time_from = "#{hour}:00"
    time_to = "#{hour}:30"
    
    times << time_from
    times << time_to
end

blocked_slots = [
  { date: Date.new(2021, 01, 04), to: "10:30" },
  { date: Date.new(2021, 01, 05), to: "11:30" },
  { date: Date.new(2021, 01, 05), to: "16:30" },
  { date: Date.new(2021, 01, 06), to: "10:30" },
  { date: Date.new(2021, 01, 06), to: "12:30" },
  { date: Date.new(2021, 01, 06), to: "18:00" }
]

initial_date = Date.new(2021, 01, 04) 
end_date = Date.new(2021, 01, 07)

(initial_date..end_date).each do |date|
    times.each_with_index do |time, i|
        next if i == times.length - 1

        time_from = times[i]
        time_to = times[i + 1]

        next if time_from == "12:00" || time_from == "12:30" || time_from == "18:00"

        booked = blocked_slots.any?{|blocked_slot| blocked_slot[:date] == date and blocked_slot[:to] == time_to}
        time_slot = TimeSlot.find_by(date: date, from_date: time_from, to_date: time_to)

        if time_slot
            time_slot.update(booked: booked)
        else
            TimeSlot.create(date: date, from_date: time_from, to_date: time_to, booked: booked)
        end
    end
end

# rails db:seed