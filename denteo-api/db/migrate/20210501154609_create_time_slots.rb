class CreateTimeSlots < ActiveRecord::Migration[6.1]
  def change
    create_table :time_slots do |t|
      t.date :date
      t.string :from_date
      t.string :to_date
      t.boolean :booked
      t.belongs_to :patient, null: true, foreign_key: true

      t.timestamps
    end
  end
end
