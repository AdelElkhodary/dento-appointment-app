class TimeSlotsController < ApplicationController
  before_action :set_time_slot, only: [:show, :update, :destroy]

  # GET /time_slots
  def index
    @from = params[:from]
    @to = params[:to]
    @booked = params[:booked]
    @time_slots = TimeSlot.where(date: @from..@to).order('from_date asc')

    time_slots_map = {}

    @time_slots.each do |time_slot|
      date = time_slot.date.strftime('%d.%m.%Y')

      unless time_slots_map[date]
        time_slots_map[date] = []
      end

      time_slots_map[date] << {
        id: time_slot.id,
        from: time_slot.from_date,
        to: time_slot.to_date,
        booked: time_slot.booked
      }
    end

    render json: time_slots_map
  end

  # GET /time_slots/1
  def show
    render json: @time_slot
  end

  # POST /time_slots
  def create
    @time_slot = TimeSlot.new(time_slot_params)

    if @time_slot.save
      render json: @time_slot, status: :created, location: @time_slot
    else
      render json: @time_slot.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /time_slots/1
  def update
    if @time_slot.booked
      render json: {error: 'Time slot already booked'}
      return
    end

    patient = Patient.create(patient_params)

    if @time_slot.update(patient: patient, booked: true)
      render json: @time_slot
    else
      render json: @time_slot.errors, status: :unprocessable_entity
    end
  end 

  # DELETE /time_slots/1
  def destroy
    @time_slot.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_time_slot
      @time_slot = TimeSlot.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def time_slot_params
      params.require(:time_slot).permit(:date, :from, :to, :booked, :patient_id)
    end

    def patient_params
      params.require(:patient).permit(:name, :phone)
    end
  
end
